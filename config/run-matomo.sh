#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_DEBUG=true
else
    DO_DEBUG=false
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
MATOMO_ADMIN_PASSWORD=${MATOMO_ADMIN_PASSWORD:-secret}
MATOMO_ADMIN_USER=${MATOMO_ADMIN_USER:-admin}
MATOMO_MYSQL_DATABASE=${MATOMO_MYSQL_DATABASE:-matomo}
MATOMO_MYSQL_HOST=${MATOMO_MYSQL_HOST:-matomo-mysql}
MATOMO_MYSQL_PASSWORD=${MATOMO_MYSQL_PASSWORD:-secret}
MATOMO_MYSQL_TABLE_PREFIX=${MATOMO_MYSQL_TABLE_PREFIX:-matomo_}
MATOMO_MYSQL_USER=${MATOMO_MYSQL_USER:-matomo}
MATOMO_ORG_NAME=${MATOMO_ORG_NAME:-Kube}
OPENLDAP_BIND_DN_PREFIX=${OPENLDAP_BIND_DN_PREFIX:-cn=matomo,ou=services}
OPENLDAP_BIND_PW=${OPENLDAP_BIND_PW:-secret}
OPENLDAP_CONF_DN_PREFIX=${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
SMTP_HOST=${SMTP_HOST:-smtp.demo.local}
SMTP_PORT=${SMTP_PORT:-25}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$OPENLDAP_USER_FILTER"; then
    OPENLDAP_USER_FILTER="(&(objectClass=inetOrgPerson)(!(pwdAccountLockedTime=*)))"
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=matomo.$OPENLDAP_DOMAIN
fi
if test -z "$SMTP_MAIL_FROM"; then
    SMTP_MAIL_FROM=matomo@$OPENLDAP_DOMAIN
fi
if test -z "$SMTP_MAIL_NOREPLY"; then
    SMTP_MAIL_NOREPLOY=noreply@$OPENLDAP_DOMAIN
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export APACHE_IGNORE_OPENLDAP=yay
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

copy_site()
{
    if test "`id -u`" = 0; then
	rsync_options="-rlDog --chown www-data:root"
	chown_with=www-data:root
    else
	rsync_options="-rlD"
	chown_with=$(id -u):$(id -g)
    fi
    if ! test -d config/; then
	rsync $rsync_options --delete /usr/src/matomo/ \
	    /var/www/html/
    else
	rm -fr /var/www/html/plugins.last
	mv /var/www/html/plugins /var/www/html/plugins.last
	rsync $rsync_options --exclude=config/ --exclude=plugins \
	    --delete /usr/src/matomo/ /var/www/html/
	for d in config plugins
	do
	    rsync $rsync_options /usr/src/matomo/$d/ \
		/var/www/html/$d/
	done
	for plugin in CustomVariables CustomPiwikJs
	do
	    sed -i "/Plugins\[\] = \"$plugin\"/d" \
		/var/www/html/config/config.ini.php
	    sed -i "/PluginsInstalled\[\] = \"$plugin\"/d" \
		/var/www/html/config/config.ini.php
	done
    fi
    find /var/www/html -type f -name '*paypal*' | while read fixme
	do
	    cat /fix.gif >"$fixme"
	done
    chown -R "$chown_with" /var/www/html || echo meh
}

if ! test -s matomo.php; then
    echo "Initializing Matomo Site Data ..."
    copy_site
fi
if ! test -s config/config.ini.php; then
    echo "Initializing Matomo Database ..."
    if test -z "$MATOMO_SALT_INIT"; then
	MATOMO_SALT_INIT=$(tr -cd '[:alnum:]' </dev/urandom | fold -w42 | head -n1)
    fi
    if test -z "$MATOMO_ADMIN_TOKEN"; then
	MATOMO_ADMIN_TOKEN=$(tr -cd '0-9a-f' </dev/urandom | fold -w32 | head -n1)
    fi
    SALTED_PW=$(php -r 'print(password_hash(md5(getenv("MATOMO_ADMIN_PASSWORD")), PASSWORD_BCRYPT));')
    if test -z "$MATOMO_TRUSTED_HOSTS"; then
	idx=1
	for h in blog whitepages codimd pad auth cloud chat diaspora mastodon
	do
	    MATOMO_TRUSTED_HOSTS="$MATOMO_TRUSTED_HOSTS $h.$OPENLDAP_DOMAIN"
	done
    fi
    idx=1
    if test -z "$TZ"; then
	TZ=UTC
    fi
    for H in $MATOMO_TRUSTED_HOSTS
    do
	h=$(echo $H | cut -d. -f1)
	if ! test -z "$MSITES"; then
	    MSITES="$MSITES,"
	fi
	MSITES="$MSITES($idx,'Kube $h','https://$h.$OPENLDAP_DOMAIN','2020-01-01 00:00:42',0,1,'','','$TZ','USD',0,'','','','','website',0,'anonymous')"
	idx=$(expr $idx + 1)
    done
    sed -e "s ADMIN_USER $MATOMO_ADMIN_USER g" \
	-e "s ADMIN_TOKEN $MATOMO_ADMIN_TOKEN g" \
	-e "s APACHE_DOMAIN $APACHE_DOMAIN g" \
	-e "s MYSQL_PFX $MATOMO_MYSQL_TABLE_PREFIX g" \
	-e "s SALTED_PW $SALTED_PW g" \
	-e "s|MATOMO_SITES_INIT|$MSITES|g" \
	-e "s LDAP_DOMAIN $OPENLDAP_DOMAIN g" \
	-e "s MATOMO_VERSION $MATOMO_VERSION g" \
	/dbseed.sql | mysql \
	    -u "$MATOMO_MYSQL_USER" \
	    "--password=$MATOMO_MYSQL_PASSWORD" \
	    -h "$MATOMO_MYSQL_HOST" \
	    "$MATOMO_MYSQL_DATABASE"
    sed -e "s MYSQL_HOST $MATOMO_MYSQL_HOST g" \
	-e "s MYSQL_PASS $MATOMO_MYSQL_PASSWORD g" \
	-e "s MYSQL_USER $MATOMO_MYSQL_USER g" \
	-e "s MYSQL_DB $MATOMO_MYSQL_DATABASE g" \
	-e "s MYSQL_PFX $MATOMO_MYSQL_TABLE_PREFIX g" \
	-e "s SALT_INIT $MATOMO_SALT_INIT g" \
	-e "s SMTP_MAIL_FROM $SMTP_MAIL_FROM g" \
	-e "s SMTP_MAIL_NOREPLY $SMTP_MAIL_NOREPLY g" \
	-e "s SMTP_PORT $SMTP_PORT g" \
	-e "s SMTP_RELAY $SMTP_HOST g" \
	/config.php >config/config.ini.php
    NEXT_RUN=$(date +%s -d "tomorrow 0:00")
    sed -e "s GEOIP_URL $GEOIP_URL g" \
	-e "s MYSQL_PFX $MATOMO_MYSQL_TABLE_PREFIX g" \
	-e "s NEXT_RUN $NEXT_RUN g" \
	-e "s SERVER_URL $PUBLIC_PROTO://$APACHE_DOMAIN g" \
	/plugin-settings.sql | mysql \
	    -u "$MATOMO_MYSQL_USER" \
	    "--password=$MATOMO_MYSQL_PASSWORD" \
	    -h "$MATOMO_MYSQL_HOST" \
	    "$MATOMO_MYSQL_DATABASE"
    echo Y | php console core:update || echo meh
else
    should_upgrade=false
    vleft=$(awk '/^## Matomo/{print $3;exit}' CHANGELOG.md 2>/dev/null)
    if test -z "$vleft"; then
	vleft=0.0.0
    fi
    vright=$(awk '/^## Matomo/{print $3;exit}' /usr/src/matomo/CHANGELOG.md 2>/dev/null)
    if test "$vright"; then
	majl=$(echo $vleft | cut -d. -f1)
	minl=$(echo $vleft | cut -d. -f2)
	pl=$(echo $vleft | cut -d. -f3)
	majr=$(echo $vright | cut -d. -f1)
	minr=$(echo $vright | cut -d. -f2)
	pr=$(echo $vright | cut -d. -f3)
	if test "$majr" -gt "$majl"; then
	    should_upgrade=true
	elif test "$majr" -eq "$majl"; then
	    if test "$minr" -gt "$minl"; then
		should_upgrade=true
	    elif test "$minr" -eq "$minl"; then
		if test "$pr" -a -z "$pl"; then
		    should_upgrade=true
		elif test "$pr" -a "$pl"; then
		    if test "$pr" -gt "$pl"; then
			should_upgrade=true
		    fi
		fi
	    fi
	fi
    fi
    if $should_upgrade; then
	copy_site
	echo Y | php console core:update || echo meh
    fi
fi

php console plugin:activate Login
if test -d /var/www/html/plugins/LoginOIDC -a "$OAUTH2_SERVER_URL" \
	-a "$OPENLDAP_DOMAIN"; then
    echo Waiting for OAUTH2 backend ...
    cpt=0
    while true
    do
	if curl --connect-timeout 5 -k \
		"$OAUTH2_SERVER_URL" >/dev/null 2>&1; then
	    echo " LemonLDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach LemonLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LLNG ... KO
	cpt=`expr $cpt + 1`
    done
    OAUTH2_AUTH_ENDPOINT=${OAUTH2_AUTH_ENDPOINT:-oauth2/authorize}
    OAUTH2_CLIENT_ID=${OAUTH2_CLIENT_ID:-matomo}
    OAUTH2_ID_MAP=${OAUTH2_ID_MAP:-sub}
    OAUTH2_SCOPE="${OAUTH2_SCOPE:-openid email}"
    OAUTH2_SECRET="${OAUTH2_SECRET:-matomo}"
    OAUTH2_TOKEN_ENDPOINT=${OAUTH2_TOKEN_ENDPOINT:-oauth2/token}
    OAUTH2_USERINFO_ENDPOINT=${OAUTH2_USERINFO_ENDPOINT:-oauth2/userinfo}
    echo "DELETE FROM ${MATOMO_MYSQL_TABLE_PREFIX}plugin_setting WHERE option_name = 'LoginOIDC';" \
	 | mysql -u "$MATOMO_MYSQL_USER" "--password=$MATOMO_MYSQL_PASSWORD" \
	    -h "$MATOMO_MYSQL_HOST" "$MATOMO_MYSQL_DATABASE" >/dev/null 2>&1
    sed -e "s MYSQL_PFX $MATOMO_MYSQL_TABLE_PREFIX g" \
	-e "s OAUTH2_AUTH_ENDPOINT $OAUTH2_AUTH_ENDPOINT g" \
	-e "s OAUTH2_CLIENT_ID $OAUTH2_CLIENT_ID g" \
	-e "s OAUTH2_CLIENT_SECRET $OAUTH2_SECRET g" \
	-e "s OAUTH2_ID_MAP $OAUTH2_ID_MAP g" \
	-e "s|OAUTH2_SCOPE|$OAUTH2_SCOPE|g" \
	-e "s OAUTH2_SERVER_URL $OAUTH2_SERVER_URL g" \
	-e "s OAUTH2_TOKEN_ENDPOINT $OAUTH2_TOKEN_ENDPOINT g" \
	-e "s OAUTH2_USER_ENDPOINT $OAUTH2_USERINFO_ENDPOINT g" \
	/oidc-settings.sql | mysql -u "$MATOMO_MYSQL_USER" \
	    "--password=$MATOMO_MYSQL_PASSWORD" \
	    -h "$MATOMO_MYSQL_HOST" "$MATOMO_MYSQL_DATABASE"
    php console plugin:activate LoginOIDC || echo mkay
elif test -d /var/www/html/plugins/LoginLdap -a "$OPENLDAP_DOMAIN"; then
    if ! grep '^\[LoginLdap\]' config/config.ini.php >/dev/null; then
	cat <<EOF >>config/config.ini.php

[LoginLdap]
use_ldap_for_authentication = 1
enable_synchronize_access_from_ldap = 0
new_user_default_sites_view_access = "all"
user_email_suffix = ""
required_member_of = ""
required_member_of_field = "memberOf"
ldap_user_filter = "$OPENLDAP_USER_FILTER"
ldap_user_id_field = "uid"
ldap_last_name_field = "sn"
ldap_first_name_field = "givenName"
ldap_alias_field = "cn"
ldap_mail_field = "mail"
ldap_password_field = "userPassword"
ldap_view_access_field = "view"
ldap_admin_access_field = "admin"
ldap_superuser_access_field = "superuser"
use_webserver_auth = 0
user_access_attribute_server_specification_delimiter = ";"
user_access_attribute_server_separator = ":"
instance_name = ""
ldap_network_timeout = 15
servers[] = "$MATOMO_ORG_NAME"
EOF
    fi
    if ! grep "^\[LoginLdap_$MATOMO_ORG_NAME\]" \
	    config/config.ini.php >/dev/null; then
	cat <<EOF >>config/config.ini.php

[LoginLdap_$MATOMO_ORG_NAME]
hostname = "$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/?sub"
port = ""
base_dn = "$OPENLDAP_BASE"
admin_user = "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE"
admin_pass = "$OPENLDAP_BIND_PW"
EOF
    fi
    php console plugin:activate LoginLdap || echo mkay
fi
if ! echo "$MATOMO_TRUSTED_HOSTS" | grep "$APACHE_DOMAIN" >/dev/null; then
    MATOMO_TRUSTED_HOSTS="$APACHE_DOMAIN $MATOMO_TRUSTED_HOSTS"
fi
for d in $MATOMO_TRUSTED_HOSTS
do
    if ! grep "^trusted_hosts.*=.*\"$d\"" config/config.ini.php \
	    >/dev/null; then
        echo INFO: adding $d to trusted hosts
	echo "trusted_hosts[] = \"$d\"" >>config/config.ini.php
    fi
done

sed -e "s HTTP_HOST $APACHE_DOMAIN g" \
    -e "s HTTP_PORT $APACHE_HTTP_PORT g" \
    -e "s SSL_CONF /etc/apache2/$SSL_INCLUDE.conf g" \
    /vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf
php console core:update --yes || echo meh

unset MATOMO_ADMIN_PASSWORD OAUTH2_TOKEN_ENDPOINT OAUTH2_USERINFO_ENDPOINT \
    MATOMO_ADMIN_USER MATOMO_MYSQL_DATABASE MATOMO_MYSQL_HOST SMTP_MAIL_FROM \
    MATOMO_MYSQL_PASSWORD MATOMO_MYSQL_TABLE_PREFIX MATOMO_MYSQL_USER \
    SMTP_MAIL_NOREPLY SMTP_PORT SMTP_RELAY OAUTH2_AUTH_ENDPOINT \
    OAUTH2_CLIENT_ID OAUTH2_ID_MAP OAUTH2_SECRET OAUTH2_SERVER_URL \
    should_upgrade MATOMO_ORG_NAME

. /run-apache.sh
