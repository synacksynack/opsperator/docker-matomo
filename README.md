# k8s Matomo

Kubernetes Matomo image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Build with:
```
$ make build
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name             |    Description              | Default                                                        | Inherited From    |
| :--------------------------- | --------------------------- | -------------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`             | Matomo ServerName           | hostname or `localhost`                                        | opsperator/apache |
|  `APACHE_IGNORE_OPENLDAP`    | Ignore LemonLDAP autoconf   | undef                                                          | opsperator/apache |
|  `APACHE_HTTP_PORT`          | Matomo HTTP(s) Port         | `8080`                                                         | opsperator/apache |
|  `PHP_ERRORS_LOG`            | PHP Errors Logs Output      | `/proc/self/fd/2`                                              | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`    | PHP Max Execution Time      | `30` seconds                                                   | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`      | PHP Max File Uploads        | `20`                                                           | opsperator/php    |
|  `PHP_MAX_POST_SIZE`         | PHP Max Post Size           | `8M`                                                           | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`   | PHP Max Upload File Size    | `2M`                                                           | opsperator/php    |
|  `PHP_MEMORY_LIMIT`          | PHP Memory Limit            | `-1` (no limitation)                                           | opsperator/php    |
|  `PUBLIC_PROTO`              | Matomo HTTP Proto           | `http`                                                         |                   |
|  `MATOMO_ADMIN_PASSWORD`     | Matomo Admin Password       | `secret`                                                       |                   |
|  `MATOMO_ADMIN_USER`         | Matomo Admin Username       | `admin`                                                        |                   |
|  `MATOMO_MYSQL_DATABASE`     | Matomo MySQL database       | `matomo`                                                       |                   |
|  `MATOMO_MYSQL_HOST`         | Matomo MySQL User           | `matomo-mysql`                                                 |                   |
|  `MATOMO_MYSQL_PASSWORD`     | Matomo MySQL Password       | `secret`                                                       |                   |
|  `MATOMO_MYSQL_TABLE_PREFIX` | Matomo MySQL Tables Prefix  | `matomo_`                                                      |                   |
|  `MATOMO_MYSQL_USER`         | Matomo MySQL User           | `matomo`                                                       |                   |
|  `MATOMO_TRUSTED_HOSTS`      | Matomo Trusted Hosts        | `blog whitepages codimd pad auth cloud chat diaspora mastodon` |                   |
|  `MATOMO_ORG_NAME`           | Matomo LDAP Org Name        | `Kube`                                                         |                   |
|  `OAUTH2_AUTH_ENDPOINT`      | OAuth2 Authorize Endpoint   | `/oauth2/authorize`                                            |                   |
|  `OAUTH2_CLIENT_ID`          | OAuth2 Client ID            | `matomo`                                                       |                   |
|  `OAUTH2_ID_MAP`             | OAuth2 ID Map               | `sub`                                                          |                   |
|  `OAUTH2_SCOPE`              | OAuth2 Scope                | `openid email`                                                 |                   |
|  `OAUTH2_SECRET`             | OAuth2 Secret               | `matomo`                                                       |                   |
|  `OAUTH2_SERVER_URL`         | OAUth2 Host Address         | undef, toggles oauth2 configuration                            |                   |
|  `OAUTH2_TOKEN_ENDPOINT`     | OAuth2 Token Endpoint       | `/oauth2/token`                                                |                   |
|  `OAUTH2_USERINFO_ENDPOINT`  | OAuth2 Userinfo Endpoint    | `/oauth2/userinfo`                                             |                   |
|  `OPENLDAP_BASE`             | OpenLDAP Base               | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local`    | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`   | OpenLDAP Bind DN Prefix     | `cn=matomo,ou=services`                                        | opsperator/apache |
|  `OPENLDAP_BIND_PW`          | OpenLDAP Bind Password      | `secret`                                                       | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX`   | OpenLDAP Conf DN Prefix     | `cn=lemonldap,ou=config`                                       | opsperator/apache |
|  `OPENLDAP_DOMAIN`           | OpenLDAP Domain Name        | `demo.local`                                                   | opsperator/apache |
|  `OPENLDAP_HOST`             | OpenLDAP Backend Address    | `127.0.0.1`                                                    | opsperator/apache |
|  `OPENLDAP_PORT`             | OpenLDAP Bind Port          | `389` or `636` depending on `OPENLDAP_PROTO`                   | opsperator/apache |
|  `OPENLDAP_PROTO`            | OpenLDAP Proto              | `ldap`                                                         | opsperator/apache |
|  `OPENLDAP_USER_FILTER`      | OpenLDAP User Filter        | `(&(objectClass=inetOrgPerson)(!(pwdAccountLockedTime=*)))`    |                   |
|  `SMTP_MAIL_FROM`            | Matomo SMTP Mail From       | `matomo@$OPENLDAP_DOMAIN`                                      |                   |
|  `SMTP_MAIL_NOREPLY`         | Matomo SMTP Mail NoReply    | `noreply@$OPENLDAP_DOMAIN`                                     |                   |
|  `SMTP_HOST`                 | Matomo SMTP Relay           | `smtp.demo.local`                                              |                   |
|  `SMTP_PORT`                 | Matomo SMTP Port            | `25`                                                           |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
|  `/var/www/html`    | Matomo Site                     |                   |
