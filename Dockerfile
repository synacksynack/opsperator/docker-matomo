FROM opsperator/php

# Matomo image for OpenShift Origin

ARG DO_UPGRADE=
ENV GEOIP_URL=http://maxmind.unetresgrossebite.com/GeoLite2-City.tar.gz \
    MATOMO_LDAP_PLUGIN_VERSION=4.3.1 \
    MATOMO_OIDC_PLUGIN_VERSION=4.0.0 \
    MATOMO_PLUGINS=https://plugins.matomo.org/api/2.0/plugins \
    MATOMO_REL_URL=https://builds.matomo.org \
    MATOMO_VERSION=4.11.0

LABEL io.k8s.description="Matomo Analytics Platform." \
      io.k8s.display-name="Matomo $MATOMO_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="matomo,piwik" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-matomo" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$MATOMO_VERSION"

USER root

COPY config/* /

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && mv /dbseed4.sql /dbseed.sql \
    && mv /config4.php /config.php \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Matomo Dependencies" \
    && apt-get -y install --no-install-recommends mariadb-client \
	postgresql-client ldap-utils openssl rsync unzip libpng16-16 \
	libjpeg62-turbo libwebp6 libgd3 \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y install --no-install-recommends libldap2-dev libzip-dev \
	libjpeg-dev libfreetype6-dev libpng-dev gnupg2 libwebp-dev \
    && debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)" \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install -j "$(nproc)" gd ldap mysqli opcache pdo_mysql \
	zip \
    && pecl install APCu-5.1.21 \
    && pecl install redis-5.3.7 \
    && docker-php-ext-enable apcu redis \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && echo "# Install Matomo" \
    && mkdir -p /usr/src/GeoIPCity /usr/src/matomo \
    && curl -fsSL -o matomo.tar.gz \
	"$MATOMO_REL_URL/matomo-$MATOMO_VERSION.tar.gz" \
    && tar -xzf matomo.tar.gz -C /usr/src/matomo --strip-components=1 \
    && curl -fsSL -o GeoIPCity.tar.gz "$GEOIP_URL" \
    && tar -xzf GeoIPCity.tar.gz -C /usr/src/GeoIPCity --strip-components=1 \
    && mv /usr/src/GeoIPCity/GeoLite2-City.mmdb /usr/src/matomo/misc/ \
    && curl -fsSL -o /tmp/loginldap.zip \
	"$MATOMO_PLUGINS/LoginLdap/download/$MATOMO_LDAP_PLUGIN_VERSION" \
    && ( \
	cd /usr/src/matomo/plugins \
	&& unzip /tmp/loginldap.zip \
	&& chmod -R o-w LoginLdap; \
    ) \
    && curl -fsSL -o /tmp/loginoidc.zip \
	"$MATOMO_PLUGINS/LoginOIDC/download/$MATOMO_OIDC_PLUGIN_VERSION" \
    && ( \
	cd /usr/src/matomo/plugins \
	&& unzip /tmp/loginoidc.zip \
	&& chmod -R o-w LoginOIDC; \
    ) \
    && echo "# Fixing permissions" \
    && for dir in /etc/lemonldap-ng /usr/src/matomo; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleanup Image" \
    && a2dismod perl \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && mv /opcache.ini /usr/local/etc/php/conf.d/ \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	matomo.tar.gz /usr/src/php.tar.xz GeoIPCity* /usr/src/GeoIPCity \
	/usr/src/matomo/CONTRIBUTING.md /usr/src/matomo/LEGALNOTICE \
	/usr/src/matomo/LICENSE /usr/src/matomo/PRIVACY.md \
	/usr/src/matomo/README.md /usr/src/matomo/SECURITY.md \
	/usr/src/matomo/tests /tmp/loginoidc.zip /tmp/loginldap.zip \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-matomo.sh"]
USER 1001
